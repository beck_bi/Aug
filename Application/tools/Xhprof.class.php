<?php

/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-13
 * @license
 * @link

 *
 */
class Xhprof
{
    protected static $flags = 0;
    protected static $options = array();
    protected static $xhprofData = array();

    /**
     * 开启调试模式
     *
     * 配置你的xhprof 你可以在php的官网看着个应用的说明
     *
     * $flags like XHPROF_FLAGS_NO_BUILTINS | XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY
     * $options like array('ignored_functions' =>  array('call_user_func',
     *                                                   'call_user_func_array')
     *                                                   )
     *                    );
     *
     * @param unknown $config
     * @throws ExtensionNotFoundException
     */
    public static function enable($config = array())
    {
    	if (!extension_loaded('xhprof')) {
    		throw new ExtensionNotFoundException(
    						'Configuration error! Make sure you have xhprof installed correctly.
                please refer http://www.php.net/manual/en/xhprof.examples.php for detail.'
    		);
    	}
    	if (!empty($config['flags'])) {
    		self::$flags = (int)$config['flags'];
    	}
    	if (!empty($config['options'])) {
    		self::$options = $config['options'];
    	}

        xhprof_enable(self::$flags, self::$options);
    }

    public static function disable()
    {
        self::$xhprofData =  xhprof_disable();
    }

    /**
     *显示调试结果
     * 你可能需要配置一个apache/nginx虚拟主机
     */
    public static function show()
    {
        self::disable();
		echo addslashes("ags'sdfa");
        include_once "xhprof_lib/utils/xhprof_lib.php";
        include_once "xhprof_lib/utils/xhprof_runs.php";
        $xhprof_runs = new XHProfRuns_Default();
        $run_id = $xhprof_runs->save_run(self::$xhprofData, "xhprof_testing");
        echo "<a href='http://pear.kang.com/xhprof_html/index.php?run={$run_id}&source=xhprof_testing' target='_blank'>see xhprof result</a>";
    }
}