<?php

/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class HelloModel extends Model
{
	protected static $table = "test_between";

	/**
	 * 初始化
	 */
	public function __construct()
	{
		parent::__construct();
		$this->obj->setDefaultDbName(DBConfig::$defaultDBName);
	}

	/**
	 *
	 */
	public function getAll()
	{
		$handle = $this->obj->getSlaveDbHandler();
		$sql = sprintf("select * from %s ", self::$table);
		$result = DBMysql::query($handle, $sql);
		return $result;
	}



}
