<?php

/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class HelloController extends Controller
{
	/**
	 * echo hello world
	 * @return void
	 */
	public function helloAction()
	{
		//HttpRequest::isAJAX();
		$title = "hello world";
		$this->assign("title",$title);
		$this->display("hello.html");
	}

	/**
	 * 连接数据库
	 */
	public function listAction()
	{
		$model = new HelloModel();
		$list = $model->getAll();
		echo "<pre>";print_r($list);
	}


}


