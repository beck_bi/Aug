<?php

require_once dirname(dirname(__FILE__)).'/system/HttpRequest.class.php';

class HttpRequestTest extends PHPUnit_Framework_TestCase
{

	public  function testGetQuery()
	{
		$this->assertEquals($_SERVER[REDIRECT_QUERY_STRING], HttpRequest::getQuery());
	}

	public function testIsGET()
	{
		$this->assertTrue(HttpRequest::isGET());
	}

	public function testIsPOST()
	{
		$this->assertTrue(HttpRequest::isPOST());
	}

	public function  testCurrentUrl()
	{
		$url = "http://www.baidu.com";
		$this->assertEquals($url, HttpRequest::currentUrl());
	}

	public function testHeader()
	{
		$this->assertEquals($_SERVER, HttpRequest::header());
	}

	public function testRedirect()
	{

	}

}


