<?php
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class AutoLoadConfig
{
	/**
	 * 获取需要自动加载的系统文件目录
	 *
	 * @return multitype:string
	 */

	public static function getSystemConfig()
	{
		return array(
				"path"=> "D:\code\language\php_project\Aug\Aug",
				"/system",
				"/config"		,
				"/lib/log",
				"/lib/db",
					);
	}

	/**
	 * 加载项目中的的类
	 */
	public static function getAppConfig(){
	    return array(
	            "path" => "D:\code\language\php_project\Aug\Application",
	            "/config",
	            "/controller",
	            "/model",
	            "/tools",
	            );

	}

	public static function getRouter()
	{
	    return array(
	                "type" => 'base',
	                "module"=> 'c',
	                "act" => 'a',
	            );
	}







}