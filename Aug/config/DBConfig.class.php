<?php
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link
 *
 */
class DBConfig
{
	public static $defaultDBName = "test";

	public static $defaultDBType = "mysql";
	#0gbk 1utf8 latin
	public static $SERVER_MASTER = array(
										"host" => "127.0.0.1",
										"port" => "3306",
										"username" => "root",
										"password" => "",
	                                    "database" => "test",
	                                    "charset" => 2
										);
	public static $SERVER_SLAVE = array(
										"host" => "127.0.0.1",
										"port" => "3306",
										"username" => "root",
										"password" => "",
	                                    "database" => "test",
	                                    "charset" => 2
										);
}