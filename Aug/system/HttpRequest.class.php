<?php
require_once 'Request.class.php';
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class HttpRequest extends Request
{

	public static function getQuery()
	{
		return $_SERVER[REDIRECT_QUERY_STRING];
	}

	public static function isGET()
	{
		return  $_SERVER["REQUEST_METHOD"] == "GET";
	}

	public static function isPOST()
	{
		return  $_SERVER["REQUEST_METHOD"] == "POST";
	}

	/**
	 * 获取当前的url
	 * @return unknown
	 */
	public static function currentUrl()
	{
		$url = '';

		$protocol = explode("/", $_SERVER["SERVER_PROTOCOL"]);
		$url .= strtolower($protocol[0])."://";
		$url .= $_SERVER["SERVER_NAME"];
		$url .= $_SERVER["REDIRECT_URL"]."?";
		$url .= $_SERVER["REDIRECT_QUERY_STRING"];

		return $url;
	}

	/**
	 * 获取http请求参数
	 * @return unknown|multitype:
	 */
	public static function getParmas()
	{
	    if(empty($_POST)){
	        return $_GET;
	    }
	    if(empty($_GET)){
	        return $_POST;
	    }
	    return array_merge($_GET, $_POST);
	}


	/**
	 * 获取head头部信息
	 * @param string $header
	 * @return unknown
	 */
	public static function header($header = null)
	{
		$key = "HTTP_".$header;
		if (array_key_exists($key, $_SERVER)) {
			return $_SERVER[$key];
		}

		return $_SERVER;
	}


	public static function redirect($url)
	{
		header("$url");
		exit;
	}

	/**
	 * 获取server的值
	 * @param string $key
	 * @return mixed
	 */
	public static function getSERVER($key = null)
	{
		if (array_key_exists($key, $_SERVER)) {
			return stripSlashes($_SERVER[$key]);
		}

		return stripSlashes($_SERVER);
	}

	/**
	 *
	 * @param string $key
	 * @return unknown
	 */
	public static function getENV($key = null)
	{
		if (array_key_exists($key, $_ENV)) {
			return stripSlashes($_ENV[$key]);
		}

		return stripSlashes($_SERVER);
	}

	/**
	 *
	 * @param string $key
	 * @return unknown
	 */
	public static function getPOST($key = null)
	{
		if (array_key_exists($key, $_POST)) {
			return stripSlashes($_POST[$key]);
		}

		return stripSlashes($_POST);
	}

	/**
	 *
	 * @param string $key
	 * @return unknown
	 */
	public static function getGET($key = null)
	{
		if (array_key_exists($key, $_GET)) {
			return stripSlashes($_GET[$key]);
		}

		return stripSlashes($_GET);
	}

	/**
	 * unset  $_GET
	 * @param string $key
	 */
	public static function unsetGet($key = null)
	{
	    if(array_key_exists($key, $_GET)){
	        unset($_GET[$key]);
	    }
	}

	/**
	 *	 获取用户ip
	 *
	 * @param string $useInt 是否将ip转为int型，默认为true
	 * @param string $returnAll 如果有多个ip时，是否会部返回。默认情况下为false
	 * @return boolean
	 */
	public static function getIp($useInt = true, $returnAll=false) {
		$ip = getenv('HTTP_CLIENT_IP');
		if($ip && strcasecmp($ip, "unknown") && !preg_match("/192\.168\.\d+\.\d+/", $ip)) {
			return $ip;
		}

		$ip = getenv('HTTP_X_FORWARDED_FOR');
		if($ip && strcasecmp($ip, "unknown")) {
			return $ip;
		}

		$ip = getenv('REMOTE_ADDR');
		if($ip && strcasecmp($ip, "unknown")) {
			return $ip;
		}

		if (isset($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
			if($ip && strcasecmp($ip, "unknown")) {
				return $ip;
			}
		}

		return false;
	}

	/**
	 * 去除反斜杠
	 * @param unknown $params
	 */
	protected static function stripSlashes(&$params)
	{
		if (is_array($params)) {
			foreach ($params as $param) {
				stripslashes($param);
			}

		} else {
			stripslashes($patams);
		}

	}
	/**
	 *
	 * @return boolean
	 */
	public static function isAJAX()
	{
		return ('XMLHttpRequest' == self::header('X_REQUESTED_WITH'));
	}

	/**
	 *
	 * @param unknown $params
	 */
	public static function getRequest($params)
	{
	    return "";
	}

	/**
	 *
	 * @param unknown $params
	 */
	public static function sendRequest($params)
	{
	    return "";
	}

}