<?php
require "Controller.class.php";
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class DispatchException extends Exception{}
class Dispatch
{
	public static $router = null;
	public static $module = null;
	public static $act = "index";
	public static $params = array();

	/**
	 * 获取模块名称
	 */
	public static function init()
	{
	    self::$router = Router::getInstance();
		self::$module = self::$router->getModule();
		self::$act = self::$router->getAct();
		self::$params = HttpRequest::getParmas();
		self::handleRouteModule();
	}

	/**
	 * 分发
	 *
	 * @param unknown $config route的配置
	 *
	 * @return obj
	 *
	 */
	public static function run($config)
	{
		self::init();
		try {
			$objName = self::$module;
			$obj = $objName::getInstance();
			call_user_func(array($obj, self::$act), self::$params);
		} catch(Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * 设置 $module $act $params
	 *
	 * @return void
	 */
	public static function handleRouteModule()
	{
	    if(empty(self::$module) || empty(self::$act)){
	        throw new DispatchException("module and act should not be empty");
	    }
		self::$module = ucwords(strtolower(self::$module))."Controller";
		self::$act = strtolower(self::$act)."Action";
	}


}