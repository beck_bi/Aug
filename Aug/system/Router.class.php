<?php

/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class RouterException extends Exception{}
class Router
{

	const ROUTER_BASE = "base";
	const ROUTER_REGEX = "regex";
	const MODULE_NAME = "module";
	const ACT_NAME = "act";
	public static $instance = null;
	public  $rules = array();
	public static $default_act = "error";

	/**
	 * 设定router规则
	 */
	public function __construct()
	{
	    $this->setRules();
	    $this->checkConfig();
	}

	/**
	 * 获取单例对象
	 * @return Router
	 */
	public static function getInstance()
	{
		if (empty(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * 设定规则
	 */
	public function setRules()
	{
	    if(empty($this->rules)){
	        $this->rules = AutoLoadConfig::getRouter();
	    }
	}

	/**
	 * 获取模块名称
	 */
	public  function getModule()
	{
	    if($this->rules["type"] == self::ROUTER_BASE) {
	        $module =  HttpRequest::getGET($this->rules[self::MODULE_NAME]);
	        HttpRequest::unsetGET($this->rules[self::MODULE_NAME]);
	        return $module;
	    }

	}

	/*
	 * 获取方法名称
	 */
	public  function getAct()
	{
	    $act = self::$default_act;
	    if($this->rules["type"] == self::ROUTER_BASE) {
	        $act = HttpRequest::getGET($this->rules[self::ACT_NAME]);
	        HttpRequest::unsetGET($this->rules[self::ACT_NAME]);
	        return $act;
	    }
	}

	/**
	 * 检查是否设置了配置
	 * @throws RouterException
	 */
	public function checkConfig()
	{
	    if(!is_array($this->rules) ||
	            !isset($this->rules["type"])||
	            !isset($this->rules["module"])||
	            !isset($this->rules["act"])

	            ){
	        throw new RouterException();
	    }

	}

}
