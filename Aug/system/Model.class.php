<?php

/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 * TODO
 */
class Model
{
	public $obj = null;

	public function __construct()
	{
		$className = "Proxy".ucwords(strtolower(DBConfig::$defaultDBType));
		$this->obj = $className::getInstance();
	}


	/**
	 * 执行 前的准备
	 */
	public function prepare()
	{


	}

	/**
	 * 执行sql
	 * @param unknown $sql
	 */
	public function execute($sql)
	{


	}

	/**
	 * 执行之后
	 */
	public function afterExecute()
	{


	}

	/**
	 * 获取所有的数据
	 */
	public function getAll()
	{

	}



}
