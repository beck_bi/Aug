<?php
require_once dirname(dirname(__FILE__)).
        DIRECTORY_SEPARATOR."config".
        DIRECTORY_SEPARATOR."TemplateConfig.class.php";

require_once dirname(dirname(__FILE__)).
        DIRECTORY_SEPARATOR."lib".
        DIRECTORY_SEPARATOR."template".
        DIRECTORY_SEPARATOR."smarty".
        DIRECTORY_SEPARATOR."Smarty.class.php";
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */

class Template
{
    const DEFAULT_TYPE = "smarty";
    private static $tpl = null;
	public static $instance = null;

	//public $render = array();

	public static function getInstanceTpl()
	{
        if(empty(self::$tpl)) {
            self::$tpl = new Smarty();
            self::$tpl->template_dir = TemplateConfig::$template_dir;
            self::$tpl->compile_dir = TemplateConfig::$compile_dir;
            self::$tpl->plugins_dir = TemplateConfig::$plugins_dir;
        }
        return self::$tpl;
	}

	/**
	 * 获取文件内容
	 * @param unknown $file
	 *
	 */
	public  static function getHtml($file)
	{
		$content = file_get_contents($file);
		return $contents;
	}


	/**
	 * 输出文件
	 * @param unknown $file
	 */
	public static function display($file)
	{
		echo self::getHtml($file);
	}

}
