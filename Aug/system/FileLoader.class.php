<?php
require_once dirname(dirname(__FILE__))."/config/AutoLoadConfig.class.php";
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class FileLoader
{
    private static $systemConfig = null;
    private static $externalConfig = null;

    /**
     * 设置系统的自动加载路径
     * @return AutoLoad
     */
    private static function setSysConfig()
    {
        self::$systemConfig = self::getPathArray(AutoLoadConfig::getSystemConfig());
    }

    /**
     * 设置用户自动加载路径
     *
     * @param unknown $config
     */
    public static function setExtConfig($config)
    {
        self::$externalConfig = self::getPathArray($config);
    }


    /**
     * 获取需要加载的文件的列表
     * @param unknown $config
     * @return boolean|multitype:Ambigous <NULL, string>
     */
    private static function getPathArray($config)
    {
        $dirctory = null;
        $includePath = null;
        $diretories = array();

        if (!empty($config) && is_array($config)) {
            //获取加载文件路径
            $dirctory = $config["path"];
            if (empty($dirctory) && !is_string($dirctory)) {
                return false;
            } else {
                unset($config["path"]);
                foreach ($config as $path) {
                    $includePath = $dirctory.$path;
                    if (is_dir($includePath))
                        $diretories[] = $includePath;
                }
            }
        }
        return $diretories;
    }



    /**
     * 用于加载系统文件
     *
     * @param string $className
     *
     * @return void
     */
    public static function sysLoad($className)
    {
        //创建单例对象
        if (empty(self::$systemConfig)) {
            self::setSysConfig();
        }
        self::includeFile(self::$systemConfig, $className);
    }

    /**
     * 自动加载外部文件
     * @param string $className
     */
    public static function extLoad($className)
    {
        self::includeFile(self::$externalConfig, $className);
    }

    /**
     *
     * @param unknown $configArray
     * @param unknown $className
     */
    public static function includeFile($configArray, $className)
    {
        if (is_array($configArray)) {
            foreach ($configArray as $directory) {
                $file = $directory.DS.$className.SUFFIX;
                if (is_file($file)) {
                    include $file;
                }
            }
        }
    }

    /**
     * 注册函数
     * @return void
     */
    public static function load($config)
    {
        if (empty(self::$externalConfig)) {
            self::setExtConfig($config);
        }
        if(empty(self::$systemConfig)) {
            self::setSysConfig();
        }
        spl_autoload_register(array("FileLoader", "sysLoad"));
        spl_autoload_register(array("FileLoader", "extLoad"));
    }

    /**
     * 手动加载可以提供直接加载和批量加载
     * @param string $config
     * @return boolean
     */
    public static function handLoad($config)
    {
        if (empty($config))
            return false;

        self::setConfg($config);

        if (is_string($config)) {
            include $config;
        }

        if (is_array($config)) {
            foreach ($config as $file) {
                include $file;
            }
        }
    }
}