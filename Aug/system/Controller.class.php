<?php
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class Controller
{
	//ajax iframe default
	public static $type = "default";
	//传值
	public static $params = array();

	public static $instance = null;


	public static $includePath = null;

	public static $tempath = null;

	//模板类
	protected  $tpl = null;

	/**
	 *
	 */
	public function __construct()
	{
	    $this->tpl = Template::getInstanceTpl();
	}

	public function getViewPath()
	{
		$viewPath = strtolower(str_replace("Controller", "", get_called_class()));
		return $viewPath.DS;
	}

	/**
	 *
	 * @return Controller
	 */
	public static function getInstance()
	{
		if (empty(self::$instance)) {
			self::$instance = new static;
		}
		return self::$instance;
	}


	/**
	 * todo add smarty yourself
	 *
	 * @param unknown $file
	 */
	protected function display($file)
	{
	    $this->tpl->display($this->getViewPath().$file);
	}

	protected  function assign($k,$v)
	{
	    $this->tpl->assign($k,$v);
	}
	/**
	 * todo add smarty yourself
	 *
	 * @param unknown $file
	 * @return string
	 */
	protected function fetch($file)
	{
		$this->tpl->fetch($this->getViewPath().$file);
	}

	/**
	 *
	 * @param unknown $message
	 */
	protected function dispalyErrorPage($message) {}

	/**
	 *
	 */
	protected function errorAction(){}

}


