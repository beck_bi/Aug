<?php

/**
 * 先转为gbk然后再urlencode
 */
function smarty_modifier_gbk_urlencode($str)
{
	$str = iconv("UTF-8", "GBK//IGNORE", $str);
	$str = urlencode($str);
    return $str;
}

?>