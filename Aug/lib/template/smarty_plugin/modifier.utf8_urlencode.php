<?php

/**
 * 先转为utf8然后再urlencode
 */
function smarty_modifier_utf8_urlencode($str)
{
	$str = iconv("GBK", "UTF-8//IGNORE", $str);
	$str = urlencode($str);
    return $str;
}

?>