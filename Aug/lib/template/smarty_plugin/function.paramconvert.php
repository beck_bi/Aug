<?php

//example {paramconvert word=���� from=gbk to=utf-8}">

function smarty_function_paramconvert($params, &$smarty)
{
	$result = $params['word'];
	$from_code = 'GBK';
	$to_code = 'UTF-8';
	$urlencode = true;
	if (isset($params['from']))
	{
		$from_code = $params['from'];
	}
	if (isset($params['to']))
	{
		$to_code = $params['to'];
	}
	if ($params['encode'] == 'false')
	{
		$urlencode = false;
	}
	if ($from_code != $to_code)
	{
		$result = iconv($from_code, $to_code . '//IGNORE', $result);
	}
	return $urlencode ? urlencode($result) : $result;
}

?>
