<?php

//example: <{wappager vt=1 type=1 page=2  totalpage=5 pagesize=10 total=91 url=list.php encoding=utf-8}>
/**
 * vt		: 1->wap1.2   2->wap2.0
 * type		: 1->列表页   2->正文分页 
 * page		: 当前页数
 * totalpage: 总页数
 * pagesize	: 每页数据，默认值为10
 * total	: 总数据记录数
 * url		: 分页链接前缀
 */

function smarty_function_wappager($params, &$smarty)
{
	
	$page		= (isset($params["page"]) && $params["page"] >= 1) ? $params["page"] : 1;
	$page_size	= (isset($params["pagesize"]) && $params["pagesize"] >= 1) ? $params["pagesize"] : 10;
	$total_page = isset($params["totalpage"]) ? intval($params["totalpage"]) : '';
	$total_num	= isset($params["total"]) ? $params['total'] : 0;
    $to_encoding = isset($params["encoding"]) ? $params['encoding'] : 'gbk';
	$page_url	= str_replace('&amp;', '&', $params["url"]);
	$page_url	= trim($page_url, '&');
	$page_url_parts = parse_url($page_url);
	$page_url_path = $page_url_parts['path'];
	$page_url = str_replace('&', '&amp;', $page_url);
	$post_params = array();
	if (!empty($page_url_parts['query']))
	{
		$tmp_v = explode('&', $page_url_parts['query']);
		foreach ($tmp_v as $v)
		{
			list($param_k, $param_v) = explode('=', $v);
			$post_params[$param_k] = iconv("UTF-8", "GBK", urldecode($param_v));
		}
	}

	$vt			= (isset($params["vt"]) && $params["vt"] >= 1) ? $params["vt"] : 1;
	$type		= (isset($params["type"]) && $params["type"] >= 1) ? $params["type"] : 1;

	if (!$page_url)
	{
		$page_url = $_SERVER['PHP_SELF'] . '?';
	}
	else if (strpos($page_url, '?') === false)
	{	
		$page_url .= '?';
	}
	if (!$total_page)
	{//根据总数据记录数和每页数据计算出总页数
		$total_page = intval($total_num / $page_size);
		if ($total_num % $page_size)
		{
			$total_page++;
		}
	}
	if ($page > $total_page)
	{
		$page = $total_page;
	}
	$html = '';
	if ($type == 1)
	{//列表页的分页
		if($total_page > 1)
		{
			if ($page < $total_page)
	        {
	            $html .= '<a href="' . $page_url . '&amp;page=' . ($page+1) . '">下页</a>&nbsp;';     
	        }
	        if ($page > 1)
	        {           
	            $html .= '<a href="' . $page_url . '&amp;page=' . ($page-1) . '">上页</a>&nbsp;'; 
	            $html .= '<a href="' . $page_url . '&amp;page=1">首页</a>&nbsp;'; 
	        }
	        if ($page < $total_page)
	        {
	            $html .= '<a href="' . $page_url . '&amp;page=' . $total_page . '">末页</a>';             
	        }
	        $html .= "<br/>";
	        if ($total_page > 9)
	        {
	            if ($vt == 1)
	            {//1.2
	                $html .= '跳到<input name="page" type="text" size="1" maxlength="3" format="*N" emptyok="true"/>页<anchor>跳转<go href="' . $page_url_path . '" accept-charset="UTF-8"  method="post"><postfield name="page" value="$(page)"/>';
	                foreach ($post_params as $k=>$v)
	                {
	                    $html .= '<postfield name="' . $k . '" value="' . $v . '" />';
	                }
	                $html .= '</go></anchor>&nbsp;&nbsp;' . "{$page}/{$total_page}页";
	            }
	            else if ($vt >= 2)
	            {//2.0  
					$temp_search  = array(">", "<","'",'"');
					$temp_replace = array("&gt;", "&lt;","&#039;","&quot;");
	                $html .= '<form action="' . $page_url . '" method="post"><div>跳到<input type="text" name="page" size="4" maxlength="4" value="" />页';
	                foreach ($post_params as $k=>$v)
	                {
						$v = @str_replace($temp_search,$temp_replace,$v);
	                    $html .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
	                }
	                $html .= '<input type="submit" value="跳转" />&nbsp;&nbsp;' . "{$page}/{$total_page}页" . '</div></form>';
	            }   
	        }
	        else
	        {
	            for ($i=1; $i<$page; $i++)
	            {
	                $html .= '<a href="' . $page_url . '&amp;page=' . $i . '">' . $i . '</a>&nbsp;'; 
	            }
	            $html .= '<a href="' . $page_url . '&amp;page=' . $page . '">[' . $page . ']</a>&nbsp;'; 
	            for ($i=$page+1; $i<=$total_page; $i++)
	            {
	                $html .= '<a href="' . $page_url . '&amp;page=' . $i . '">' . $i . '</a>&nbsp;'; 
	            }
	        }
		}
		
	}
	else if ($type == 2)
	{//正文分页
		if ($total_page > 1)
		{
			if ($page < $total_page)
			{
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page+1) . '">下页</a>&nbsp;'; 
			}
			if ($page < $total_page - 1)
			{
				$html .= '<a href="' . $page_url . '&amp;page=rest' . $page . '">余下全文</a>&nbsp;'; 
			}
			if ($page > 1)
			{
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page-1) . '">上页</a>&nbsp;'; 
			}
//			if ($page > 1)
//	        {           
//	            $html .= '<a href="' . $page_url . '&amp;page=' . ($page-1) . '">上页</a>&nbsp;'; 
//	            $html .= '<a href="' . $page_url . '&amp;page=1">首页</a>&nbsp;'; 
//	        }
	        if ($page < $total_page)
	        {
	            $html .= '<a href="' . $page_url . '&amp;page=' . $total_page . '">末页</a>';             
	        }
			$html .= "<br/>";
			if ($page - 4 > 0)
			{
				$html .= '<a href="' . $page_url . '&amp;page=1">1</a>&nbsp;'; 
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page-2) . '">...</a>&nbsp;'; 				
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page-1) . '">' . ($page-1) . '</a>&nbsp;'; 
//				if ($vt == 1)
//	            {//1.2
//	                $html .= '<br/>跳到<input name="page" type="text" size="1" maxlength="3" format="*N" emptyok="true"/>页<anchor>跳转<go href="' . $page_url_path . '" accept-charset="UTF-8"  method="post"><postfield name="page" value="$(page)"/>';
//	                foreach ($post_params as $k=>$v)
//	                {
//	                    $html .= '<postfield name="' . $k . '" value="' . $v . '" />';
//	                }
//	                $html .= '</go></anchor>&nbsp;&nbsp;' . "{$page}/{$total_page}页";
//	            }
//	            else if ($vt == 2)
//	            {//2.0  
//	                $html .= '<form action="' . $page_url . '" method="post"><div>跳到<input type="text" name="page" size="4" maxlength="4" value="" />页';
//	                foreach ($post_params as $k=>$v)
//	                {
//	                    $html .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
//	                }
//	                $html .= '<input type="submit" value="跳转" />&nbsp;&nbsp;' . "{$page}/{$total_page}页" . '</div></form>';
//	            }   
			}
			else
			{
				for ($i=1; $i<$page; $i++)
				{
					$html .= '<a href="' . $page_url . '&amp;page=' . $i . '">' . $i . '</a>&nbsp;'; 
				}
//				$html .= '<a href="' . $page_url . '&amp;page=' . $page . '">[' . $page . ']</a>&nbsp;'; 
//				for ($i=$page+1; $i<=$total_page; $i++)
//	            {
//	                  $html .= '<a href="' . $page_url . '&amp;page=' . $i . '">' . $i . '</a>&nbsp;'; 
//	            }
			}
			$html .= '<a href="' . $page_url . '&amp;page=' . $page . '">[' . $page . ']</a>&nbsp;'; 
			if ($page + 4 <= $total_page)
			{
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page+1) . '">' . ($page+1) . '</a>&nbsp;'; 
				$html .= '<a href="' . $page_url . '&amp;page=' . ($page+2) . '">...</a>&nbsp;'; 
				$html .= '<a href="' . $page_url . '&amp;page=' . $total_page . '">' . $total_page . '</a>&nbsp;'; 
			}
			else
			{
				for ($i=$page+1; $i<=$total_page; $i++)
				{
					$html .= '<a href="' . $page_url . '&amp;page=' . $i . '">' . $i . '</a>&nbsp;'; 
				}
			}
		   
		}
	}
	//增加转码
	if ('gbk' != $to_encoding) {
	    $html = mb_convert_encoding($html, $to_encoding, 'GBK');
	}
	return $html;
}


?>