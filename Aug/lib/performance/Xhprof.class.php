<?php
class Xhprof
{
    protected $flags = 0;
    protected $options = array();
    protected $xhprofData = array();

    public function __construct($config = array())
    {
        if (!extension_loaded('xhprof')) {
            throw new ExtensionNotFoundException(
                'Configuration error! Make sure you have xhprof installed correctly.
                please refer http://www.php.net/manual/en/xhprof.examples.php for detail.'
            );
        }
        if (!empty($config['flags'])) {
            $this->flags = (int)$config['flags'];
        }
        if (!empty($config['options'])) {
            $this->options = $config['options'];
        }
    }

    public function enable()
    {
        xhprof_enable($this->flags, $this->options);
    }

    public function disable()
    {
        $this->xhprofData =  xhprof_disable();
    }

    public function show()
    {
        $this->disable();

        include_once "xhprof_lib/utils/xhprof_lib.php";
        include_once "xhprof_lib/utils/xhprof_runs.php";
        $xhprof_runs = new XHProfRuns_Default();
        $run_id = $xhprof_runs->save_run($this->xhprofData, "xhprof_testing");

        echo "<a href='http://pear.kang.com/xhprof_html/index.php?run={$run_id}&source=xhprof_testing' target='_blank'>see xhprof result</a>";
    }
}