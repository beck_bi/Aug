<?php
require "Logger.class.php";
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class SimpleLogger extends Logger
{

	private static $logPath = "./temp";

	/**
	 * 日志
	 * @param unknown $message
	 * @param unknown $level
	 * @param unknown $catagory
	 * @return boolean
	 */
	public static function log($message, $level, $catagory)
	{
		$logMessage = parent::log($message, $level, $catagory);
		return self::_log($logMessage);
	}

	/**
	 * 往文件里面写日志
	 * @param unknown $logMessage
	 * @return boolean
	 */
	private static function _log($logMessage)
	{
		$logFile = self::$logPath.DIRECTORY_SEPARATOR.strtotime("today").".log";

		if (!is_dir(self::$logPath)) {
			if (!mkdir(self::$logPath, 0755)) {
				return false;
			}
		} else {
			$fp = @fopen($logFile, "a+");
			if (!$fp) {
				return false;
			}
			$logMessage .= "\n";
			fputs($fp, $logMessage);
			fclose($fp);
		}
	}

}

/*
$message = "hello"; $level="info"; $catagory = "boy";
SimpleLogger::log($message, $level, $catagory);
SimpleLogger::showLog($message, $level, $catagory);
*/