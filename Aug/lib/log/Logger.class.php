<?php
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class Logger
{
	protected static $config = null;

	/**
	 * 设定配置文件
	 * @param unknown $config
	 */
	protected static function setConfig($config)
	{
		if (!empty($config)) {
			self::$config = $config;
		}
	}

	/**
	 * 获取配置文件
	 * @return unknown
	 */
	protected static function getConfig()
	{
		return self::$config;
	}

	/**
	 * 格式化输出一条日志
	 * @param unknown $message
	 * @param unknown $level
	 * @param unknown $catagory
	 * @return string
	 */
	protected static function log($message, $level, $catagory)
	{
		$logMessage = null;

		//日志的格式
		$date = date("Y-m-d H:i:s", time());
		$logMessage .= "date:$date # message:$message";
		$logMessage .= " # level:$level catagory: $catagory";

		return $logMessage;
	}


	public static function showLog($message, $level, $catagory)
	{
		echo Logger::log($message, $level, $catagory);
	}

}

