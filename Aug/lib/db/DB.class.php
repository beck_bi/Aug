<?php
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class DBEncode{
	// 数据库编码相关
	const ENCODING_GBK      = 0; ///< GBK 编码定义
	const ENCODING_UTF8     = 1; ///< UTF8 编码定义
	const ENCODING_LATIN    = 2; ///< LATIN1 编码定义
}

abstract  class DB{

	protected static $handleArray = array();

	protected static function getHandleKey($params) {
		ksort($params);
		return md5(implode("_", $params));
	}
	abstract  static function createHandle($dbconfigArray, $DBName, $encode);
	abstract  static function releaseHandle($handle);
	abstract  static function execute($handle, $sql);
}