<?php
require "DB.class.php";
/**
 *
 *
 * Beck Confidential
 * Copyright (c) 2013, Beck Corp. <Beck.Bi>.
 * All rights reserved.
 *
 * PHP version 5
 *
 * @category  Aug
 * @package package_name
 * @author beck
 * @date 2013-8-10
 * @license
 * @link

 *
 */
class DBMysql extends DB{

	/**
	 * 获取连接句柄
	 * @param unknown $dbconfigArray
	 * @param unknown $DBName
	 * @param unknown $encode
	 * @return boolean|multitype:|unknown
	 */
    public static function createHandle($dbconfigArray, $DBName, $encode = DBEncode::ENCODING_UTF8)
    {
        if(empty($dbconfigArray) || empty($DBName)) {
            return false;
        }

        $handleKey = self::getHandleKey($dbconfigArray);

        if(isset(self::$handleArray[$handleKey])) {
            return self::$handleArray[$handleKey];
        }

        $port = 3306;
        do{
            if(!array_key_exists('host', $dbconfigArray)) {
                break;
            }
            if(!array_key_exists('username', $dbconfigArray)) {
                break;
            }
            if(!array_key_exists('password', $dbconfigArray)) {
                break;
            }
            if(array_key_exists('port', $dbconfigArray)) {
                $port = (int) $dbconfigArray['port'];
                if($port < 1024 || $port > 65535){
                    break;
                }
            }
            $host = $dbconfigArray['host'];
            if(strlen($host) == 0){
                break;
            }
            $username = $dbconfigArray['username'];
            if(strlen($username) == 0){
                break;
            }
            $password = $dbconfigArray['password'];
            /*
            if(strlen($password) == 0){
                break;
            }
            */

            $link = @mysqli_connect($host, $username, $password, $DBName, $port);
            if(!$link){
                for($i=0; $i< 3 &&$link === false; $i++) {
                    usleep(600000);
                    $link = @mysqli_connect($host, $username, $password, $DBName, $port);
                }
            }

            if($link === false) {
                break;
            }

            $isSetEncode = true;

            switch($encode) {
                case DBEncode::ENCODING_UTF8:
                    $isSetEncode = mysqli_query($link, "set names utf8" );
                    break;
                case DBEncode::ENCODING_GBK:
                    $isSetEncode = mysqli_query($link, "set names gbk");
                    break;
                case DBEncode::ENCODING_LATIN:
                    $isSetEncode = mysqli_query($link, "set names latin1");
                    break;
            }

            if($isSetEncode === false) {
                mysqli_close($link);
                break;
            }
            self::$handleArray[$handleKey] = $link;
            return $link;
        }while(false);
        // log it
        return false;
    }

    /**
     * 释放句柄
     * @param unknown $handle
     */
    public static function releaseHandle($handle)
    {
        if($handle === false){
            return ;
        }
        foreach ( self::$handleArray as $handleKey=>$handleObject) {
            if($handleObject->thread_id == $handle->thread_id ) {
                unset(self::$handleArray[$handleKey]);
            }
        }
        mysqli_close($handle);
    }

    /**
     * 执行sql语句
     * @param unknown $handle
     * @param unknown $sql
     * @return boolean
     */
    public static function execute($handle, $sql)
    {
        if($handle === false) {
            return false;
        }
        if(mysqli_query($handle, $sql)) {
            return true;
        }
        //log
        return false;
    }

    /**
     * 查询
     * @param unknown $handle
     * @param unknown $sql
     * @return boolean|multitype:unknown
     */
    public static function query($handle, $sql)
    {
        if($handle === false) {
            return false;
        }
        do{
            $res = array();
            if(($query = mysqli_query($handle, $sql)) === false) {
                return false;
            }
            while($row = mysqli_fetch_assoc($query)) {
                $res[] = $row;
            }
            mysqli_free_result($query);
            return $res;
        }while(false);
        return false;
    }

    /**
     * 获取第一条数据
     * @param unknown $handle
     * @param unknown $sql
     * @return boolean|unknown
     */
    public static function queryFirst($handle, $sql)
    {
        if($handle === false) {
            return false;
        }
        do{
            if(($query = mysqli_query($handle, $sql)) === false) {
                return false;
            }
            $row = mysqli_fetch_assoc($query);
            mysqli_free_result($query);
            return $row;
        }while(false);
        return false;
    }
}

/*
$dbconfigArray = array("host"=> "localhost", "port"=> 3306, "username"=>"root", "password" => "583038342");
$DBName = "sqltest";
$handle = DBMysql::getHandle($dbconfigArray, $DBName);
$sql = "select * from goods";
$result = DBMysql::query($handle, $sql);
var_dump(DBMysql::releasHandle($handle));
*/

