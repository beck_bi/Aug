<?php
//.class.php个人偏好
defined('SUFFIX') or define('SUFFIX','.class.php');
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('PS') or define('PS', PATH_SEPARATOR);
//设置调试模式On Off
define('AGU_DEBUG', "On");

ini_set("display_errors", AGU_DEBUG);

//报告错误
if (AGU_DEBUG == AGU_DEBUG) {
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
} else {
	error_reporting(NULL);
}


//设置错误处理器
function aug_error_handler($errno, $errorstr)
{
	header("Location:/404.html");
	die;
}
if(AGU_DEBUG != AGU_DEBUG)
    set_error_handler("aug_error_handler");


//设置异常处理器
function aug_exception_handler($exception)
{
	header("Location:/404.html");
	die;
}
if(AGU_DEBUG != AGU_DEBUG)
    set_exception_handler("aug_exception_handler");


//自动加载文件
require dirname(__FILE__).'/system/FileLoader'.SUFFIX;


$userConfig["included"] = empty($userConfig["included"])?null:$userConfig["included"];
FileLoader::load($userConfig["included"]);
FileLoader::load(AutoLoadConfig::getAppConfig());
Dispatch::run($userConfig);









